# Rust showcase presentation

This repository contains the slides for the presentation.

---

<div align="center">

✨ [Live version](https://n.ethz.ch/~brgerhar/presentations/2024-04-09-rust-projects)

</div>

---

## Generating the slides

The [reveal.js](https://revealjs.com/) slides can be generated from
the Markdown file with [pandoc](https://pandoc.org/) using the
following command:

```bash
pandoc \
  -o index.html \
  --standalone \
  -t revealjs \
  --slide-level=2 \
  --shift-heading-level-by=1 \
  -V transition=none \
  -V revealjs-url=https://unpkg.com/reveal.js@4.2.1 \
  -H header.html \
  index.md
```

or by using the [Makefile](Makefile):

```sh
make
```
