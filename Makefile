index.html: index.md header.html Makefile
	pandoc \
	 -o $@ \
	 --standalone \
	 -t revealjs \
	 --slide-level=2 \
	 --shift-heading-level-by=1 \
	 -V transition=none \
	 -V revealjs-url=https://unpkg.com/reveal.js@4.2.1 \
	 -H header.html \
	 $<

# 	 --embed-resources \
