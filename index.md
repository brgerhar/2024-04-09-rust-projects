---
author: Gerhard Bräunlich
date: April 09, 2024
lang: en
title: Rust projects
...

---

# Simulation: Downhole heat exchanger

[gitlab.com/g-braeunlich/dhe](https://gitlab.com/g-braeunlich/dhe)

- Numerics
- GUI
- CLI
- Cross compile to 🪟
- python 🐍 interface using [pyO3](https://pyo3.rs/)

---

- 👍 Near C performance
- 👍 Cool experience letting a rust macro write the GUI boilerplate
  code
- 👍 Very comfortable 🐍 bindings
- 👎 GUI is still a bit of a moving target in 🦀

---

## GUI

::: {.r-hstack .r-stretch}

```rs
/// Downhole heat exchanger
#[derive(metastruct::MetaStruct)]
pub struct DHE {
    /// x coordinate [m]
    pub x: f64,
    /// y coordinate [m]
    pub y: f64,
    /// L: Length [m]
    pub L: f64,
    /// ⌀: Diameter [m]
    pub D: f64,
    ...
```

![](img/dhe-gui.png)

:::

---

## CLI

::: {.r-hstack .extend}

```rs
#[derive(Debug, StructOpt)]
#[structopt(about="DHE command line interface")]
struct Cli {
  /// Output file
  #[structopt(short, long, parse(from_os_str))]
  out: Option<PathBuf>,
  /// Input file
  #[structopt(parse(from_os_str))]
  input: PathBuf,
}
```

```
$ dhe-cli --help
DHE command line interface

Usage: dhe-cli [OPTIONS] <INPUT>

Arguments:
  <INPUT>  Input file

Options:
  -o, --out <OUT>  Output file
  -h, --help       Print help
```

:::

---

## 🐍 bindings

```rs
#[pymodule]
/// Python bindings for DHE.
fn dhe(py: Python, m: &PyModule) -> PyResult<()> {
    #[pyfn(m)]
    #[pyo3(name = "calc_P")]
    /// Compute temeratures for sink, source and pipe
    /// for given power values `P` at times `t` and parameters `dhe` / `env`.
    fn calc_P(
        py: Python,
        t: PyReadonlyArray1<f64>,
        P: PyReadonlyArray1<f64>,
        dhe: Foreign<Vec<libdhe::DHE>>,
        env: GlobalParameters,
        precision: f64,
    ) -> PyResult<(Py<PyArray2<f64>>, Py<PyArray2<f64>>, Py<PyArray4<f64>>)> {
        let GlobalParameters { env, g_method, T_brine_method } = env;
        ...
    }
    Ok(())
}
```

---

```py
import dhe
help(dhe.calc_P)
```

```
Help on built-in function calc_P in module dhe:

calc_P(t, P, dhe, env, precision)
    Compute temeratures for sink, source and pipe for given power values `P` at times `t` and parameters `dhe` / `env`.
```

# Kugel

<section data-background-iframe="https://g-braeunlich.gitlab.io/kugel/" data-background-interactive>
 <div class="label-box">
  <h4>[gitlab.com/g-braeunlich/kugel](https://gitlab.com/g-braeunlich/kugel)</h4>
  <ul>
   <li>Numerics</li>
   <li>wasm (Web Assembly)</li>
  </ul>
 </div>
</section>

---

- 👍 Portability (Compile native / wasm / cross compile)
- 👍 Good experience with traits to abstract drawing to different
  backends (web / native)

---

## Abstraction: traits

```rs
pub trait Canvas {
    fn draw_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, line_style: &LineStyle);
    fn draw_rectangle_filled_with_color(&mut self, x: f64, y: f64, w: f64, h: f64, color: &Color);
...
```

# Sola HQ

Repo: not yet released

- Full stack rust web site, serving a wasm bundle

---

<section data-background-iframe="https://inforunners.id.ethz.ch/de/event/sola-2024" data-background-interactive>
 <div class="label-box">
  <h4>Sola HQ</h4>
 </div>
</section>

---

# Matrix LLM bot

[gitlab.ethz.ch/sis/experimental/matrix-llm-bot](https://gitlab.ethz.ch/sis/experimental/matrix-llm-bot)

![](img/matrix-llm-bot.svg)

- Matrix bot communicating with a
  [llamacpp](https://github.com/ggerganov/llama.cpp) server.
- Containerized as a musl binary in a
  [scratch](https://hub.docker.com/_/scratch/) image (empty image)

---

- 👍 Much more robust than 🐍!
- 👍 Very small container sizes (14MB) (can put a musl binary into a
  [scratch](https://hub.docker.com/_/scratch/), i.e. empty docker container)

---

```Dockerfile
FROM clux/muslrust:stable as build

COPY Cargo.* /volume/
COPY src /volume/src
RUN cargo install --root /build/ --path .


FROM scratch
COPY --from=build /build/bin/matrix-llm-bot /bin/
CMD ["/bin/matrix-llm-bot", "/etc/matrix-llm-bot/config.toml"]
```

---

## The End

[![](https://imgs.xkcd.com/comics/carcinization.png)](https://xkcd.com/2314/){title="Nature abhors a vacuum and also anything that's not a crab."}

<small>[Carcinization](https://xkcd.com/2314/)</small>

::: fragment

📊 Slides: ✨ [Live
  version](https://n.ethz.ch/~brgerhar/presentations/2024-04-09-rust-projects)
  / 📖 [Source](https://gitlab.ethz.ch/brgerhar/2024-04-09-rust-projects)

:::
